---
title: Use Hakyll with Codeberg Pages
author: TM Zhang
tags: utility, hakyll
---

# Solution

The Hakyll ouputs files (including the 'real' `index.html`) to a `_site` subdirectory by default, 
while Codeberg Pages assumes that `index.html` is located at `/`. 
Meanwhile, Hakyll will search files in `/` to compile by default.
So one has to create a folder (say `assets/`), move the files into it and modify `site.hs` like this:

```haskell
config :: Configuration
config = defaultConfiguration
  { destinationDirectory = "."
  , providerDirectory = "assets"
  }

main :: IO ()
main = hakyllWith config $ do
  ...
```

___Note:___ One may not use `stack exec site rebuild` or `stack exec site clean`
after changing the contents of the website, because now the `destinationDirectory`
is at the root, the `clean` operation will remove the whole repository. The former
files shall be cleaned manually or via a script.

## Reference

- [1] [TUTORIAL: USING HAKYLL WITH GITHUB PAGES](https://jaspervdj.be/hakyll/tutorials/github-pages-tutorial.html)
- [2] [Hakyll.Core.Configuration's Documentation](https://jaspervdj.be/hakyll/reference/Hakyll-Core-Configuration.html)
