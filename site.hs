--------------------------------------------------------------------------------
{-# LANGUAGE OverloadedStrings #-}
import           Data.Monoid (mappend)
import           Hakyll
import           Text.Pandoc.Options
import           Text.Pandoc.Definition
import           Text.Pandoc.Walk (walk, walkM)
import           Control.Monad ((>=>))
import           Hakyll.Core.Compiler
import           Data.ByteString.Lazy.Char8 (pack, unpack)
import qualified Network.URI.Encode as URI (encode)
import qualified Data.Text as Text
import           Text.Pandoc.Highlighting (Style, styleToCss, tango)
import           Text.Pandoc.SideNote
--------------------------------------------------------------------------------
main :: IO ()
main = hakyllWith config $ do
    match "images/*" $ do
        route   idRoute
        compile copyFileCompiler

    match "css/*" $ do
        route   idRoute
        compile compressCssCompiler

    match "css/tufte-pandoc-css/docs/*.css" $ do
        route   idRoute
        compile compressCssCompiler
    
    match "css/tufte-pandoc-css/docs/et-book/*/*" $ do
        route   idRoute
        compile copyFileCompiler

    -- match "css/tufte-pandoc-css/docs/et-book/et-book-bold-line-figures/*" $ do
    --     route $ cutTheseWith [2,3] "/"
    --     compile copyFileCompiler
    --
    -- match "css/tufte-pandoc-css/docs/et-book/et-book-display-italic-old-style-figures/*" $ do
    --     route $ cutTheseWith [2,3] "/"
    --     compile copyFileCompiler
    --
    -- match "css/tufte-pandoc-css/docs/et-book/et-book-roman-line-figures/*" $ do
    --     route $ cutTheseWith [2,3] "/"
    --     compile copyFileCompiler
    --
    -- match "css/tufte-pandoc-css/docs/et-book/et-book-roman-old-style-figures/*" $ do
    --     route $ cutTheseWith [2,3] "/"
    --     compile copyFileCompiler
    --
    -- match "css/tufte-pandoc-css/docs/et-book/et-book-semi-bold-old-style-figures/*" $ do
    --     route $ cutTheseWith [2,3] "/"
    --     compile copyFileCompiler

    match (fromList ["about.md"]) $ do
        route   $ setExtension "html"
        compile $ myPandocCompiler
            >>= loadAndApplyTemplate "templates/default.html" defaultContext
            >>= katexFilter
            -- >>= relativizeUrls

    -- build up tags, see https://javran.github.io/posts/2014-03-01-add-tags-to-your-hakyll-blog.html
    tags <- buildTags "posts/*" (fromCapture "tags/*.html")
    tagsRules tags $ \tag pattern -> do
            let title = "Posts tagged \"" ++ tag ++ "\""
            route idRoute
            compile $ do
                posts <- recentFirst =<< loadAll pattern
                let ctx = constField "title" title
                          `mappend` listField "posts" (postCtxWithTags tags) (return posts)
                          `mappend` defaultContext
    
                makeItem ""
                    >>= loadAndApplyTemplate "templates/tag.html" ctx
                    >>= loadAndApplyTemplate "templates/default.html" ctx
                    >>= katexFilter
                    -- >>= relativizeUrls

    match "posts/*" $ do
        route $ setExtension "html"
        compile $ myPandocCompiler
            >>= loadAndApplyTemplate "templates/post.html"    (postCtxWithTags tags)
            >>= loadAndApplyTemplate "templates/default.html" (postCtxWithTags tags)
            >>= katexFilter
            -- >>= relativizeUrls

    create ["archive.html"] $ do
        route idRoute
        compile $ do
            posts <- recentFirst =<< loadAll "posts/*"
            let archiveCtx =
                    listField "posts" postCtx (return posts) `mappend`
                    constField "title" "Archives"            `mappend`
                    defaultContext

            makeItem ""
                >>= loadAndApplyTemplate "templates/archive.html" archiveCtx
                >>= loadAndApplyTemplate "templates/default.html" archiveCtx
                >>= relativizeUrls

    match "pluto_archive/*" $ do
        route idRoute
        compile copyFileCompiler

    match "index.html" $ do
        route idRoute
        compile $ do
            posts <- recentFirst =<< loadAll "posts/*"
            let indexCtx =
                    listField "posts" postCtx (return posts) `mappend`
                    defaultContext

            getResourceBody
                >>= applyAsTemplate indexCtx
                >>= loadAndApplyTemplate "templates/default.html" indexCtx
                >>= relativizeUrls

    match "templates/*" $ compile templateBodyCompiler

    create ["css/syntax.css"] $ do
        route idRoute
        compile $ do
            makeItem $ styleToCss pandocCodeStyle

--------------------------------------------------------------------------------
postCtx :: Context String
postCtx =
    dateField "date" "%F" `mappend`
    defaultContext

postCtxWithTags :: Tags -> Context String
postCtxWithTags tags = tagsField "tags" tags <> postCtx

-- For code highlighting, see https://rebeccaskinner.net/posts/2021-01-31-hakyll-syntax-highlighting.html
pandocCodeStyle :: Style
pandocCodeStyle = tango

-- For using tikz in hakyll, see https://taeer.bar-yam.me/blog/posts/hakyll-tikz/
tikzFilter :: Block -> Compiler Block
tikzFilter (CodeBlock (id, "tikzpicture":extraClasses, namevals) contents) =
  (imageBlock . ("data:image/svg+xml;utf8," ++) . URI.encode . filter (/= '\n') . itemBody <$>) $
    makeItem (Text.unpack contents)
     >>= loadAndApplyTemplate (fromFilePath "templates/tikz.tex") (bodyField "body")
     >>= withItemBody (return . pack
                       >=> unixFilterLBS "rubber-pipe" ["--pdf"]
                       >=> unixFilterLBS "pdftocairo" ["-svg", "-", "-"]
                       >=> return . unpack)
  where imageBlock fname = Para [Image (id, "tikzpicture":extraClasses, namevals) [] (Text.pack fname, "")]
tikzFilter x = return x


myPandocCompiler :: Compiler (Item String)
myPandocCompiler = pandocCompilerWithTransformM readerOptions writerOptions $ walkM tikzFilter . usingSideNotes
    where readerOptions = defaultHakyllReaderOptions
          writerOptions = defaultHakyllWriterOptions { writerHTMLMathMethod = KaTeX ""
                                                     -- , writerNumberSections = True
                                                     , writerHighlightStyle = Just pandocCodeStyle
                                                     }

config :: Configuration
config = defaultConfiguration
    { destinationDirectory = "."
    , providerDirectory = "assets"
    }

katexFilter :: Item String -> Compiler (Item String)
katexFilter = withItemBody (unixFilter "./katex_cli" [])

cutTheseWith ::[Integer] -> Text.Text -> Routes
cutTheseWith ns s = customRoute 
                  $ Text.unpack 
                  . mconcatWith s 
                  .throwfromNumber ns 
                  . Text.splitOn s 
                  . Text.pack 
                  . toFilePath

throwfromNumber :: [Integer] -> [a] -> [a]
throwfromNumber ns xs = fmap snd 
                      $ filter (\(n,_) -> n `notElem` ns) 
                      $ zip [1..] xs

mconcatWith :: Monoid a => a -> [a] -> a
mconcatWith m = foldr1 (\x y -> x<>m<>y)
